# Workflow

1. Fork the project into your own Gitlab account
2. Clone the project to your local machine
3. Create a new branch for the feature you want to work on

`git checkout -b <name-of-branch>`

4. Make your changes
5. Add and commit your local changes
6. Push your changes to your fork on Gitlab
7. Open a *Merge Request* on the repository you want your changes merged into
8. ???
9. Profit.
