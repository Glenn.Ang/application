# KSWS: SB (for lack of a better name)

Our task was to analyze the airways of different patients and to compare them with each other. For each patient, we were give around 500 images from CT scans and first had to come up with a suitable data structure to save them in. We used the free software *ImageJ* that offers a very helpful *skeletonize* - algorithm for image sequences https://imagej.net/AnalyzeSkeleton. This algorithm created a graph/tree for each patient, whose vertices and edges we would save in individual .json-files. The whole point of these files was so that we wouldn't have to run *ImageJ* every time we wanted to access the skeletonized lungs and so that we could manipulate them with our own programs written in Python. 

Basically all of our implementations and results can be found in the directory `skeleton`. 

