importPackage(Packages.ij);
importPackage(Packages.sc.fiji.analyzeSkeleton);
 
// Takes a binary image as input
var imp = IJ.getImage(); // get current open image

// hier unter Umständen slices=xxx an die Anzahl der Images pro Patient anpassen
IJ.run(imp, "Properties...", "channels=1 slices=851 frames=1 unit=mm pixel_width=0.6940000 pixel_height=0.6940000 voxel_depth=0.5");




 
// Skeletonize the image
IJ.run(imp, "Skeletonize (2D/3D)", "");
 
// Initialize AnalyzeSkeleton_
var skel = new AnalyzeSkeleton_();
skel.calculateShortestPath = true;
skel.setup("", imp);
 
// Perform analysis in silent mode
// (work on a copy of the ImagePlus if you don't want it displayed)
// run(int pruneIndex, boolean pruneEnds, boolean shortPath, ImagePlus origIP, boolean silent, boolean verbose)
var skelResult = skel.run(AnalyzeSkeleton_.NONE, false, true, null, true, false);
 
// Read the results

var str = "";
str += "{";

var graph = skelResult.getGraph()[0];
var root = graph.getRoot();
str += '"root": "' + root.pointsToString() + '", ';

var endpoint_voxels = skelResult.getListOfEndPoints();
var junction_voxels = skelResult.getListOfJunctionVoxels();
var vertices = graph.getVertices();


var step = 0;
str += '"vertices": [';
for (step = 0; step < vertices.length; step++) {
	if (step > 0) str += ", ";
	var vertex = vertices[step];
	str += "{" + '"points":"' + vertex.pointsToString() + '", "type":' ;
	var point = vertex.getPoints()[0];
	if (endpoint_voxels.contains(point)) {
		str +=  '"endpoint"';
	} else if (junction_voxels.contains(point)) {
		str +=  '"junction"';
	}
        str += ', "lobe":"none"';
	str += "}";
}
str += "], ";

var edges = graph.getEdges();
var step = 0;
str += '"edges": [';
for (step = 0; step < edges.length; step++) {
	if (step > 0) str += ", ";
	var edge = edges[step];
	str += '{"vertex1": "' + edge.getV1().pointsToString() + 
	'", "vertex2": "' + edge.getV2().pointsToString() + '", "points":';
	slabs = edge.getSlabs();
	str += '"' + slabs.toString() + '"';
	str += ', "length":' + edge.getLength().toString();
	str += "}";
}


str += "]";
str += "}";
IJ.log(str);

