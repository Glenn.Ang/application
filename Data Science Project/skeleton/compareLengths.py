from graph import Graph
import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os

def getDistance(v1, v2):
   x1, y1, z1 = getMiddlePoint(v1)
   x2, y2, z2 = getMiddlePoint(v2)   
   return math.sqrt(math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2) + math.pow(z2 - z1, 2)* 1.0) 

def getMiddlePoint(vertex):
   points = getVertexPoints(vertex)
   points_n = len(points)
   x = 0.0
   y = 0.0
   z = 0.0
   for point in points:
      x += point[0]
      y += point[1]
      z += point[2]
   return (x/points_n, y/points_n, z/points_n)

def getVertexPoints(vertex):
   points = []
   starts = []
   endings = []
   commas = []
   counter = 0
   while counter < len(vertex):
      if vertex[counter] == ',':
         commas.append(counter)
      elif vertex[counter] == '(':
         starts.append(counter)
      elif vertex[counter] == ')':
         endings.append(counter)
      counter += 1
   for i in range(len(starts)):
      x_coord = float(vertex[starts[i]+1:commas[2*i]])
      y_coord = float(vertex[commas[2*i]+2:commas[2*i+1]])
      z_coord = float(vertex[commas[2*i+1]+2:endings[i]])
      points.append((x_coord, y_coord, z_coord))
   return points

graphs = []

for i in range(62):
   f = open("patients/good/" + str(i) + ".json", "r")
   data = f.read()
   g = Graph(data)
   graphs.append(g)
   f.close()

graphs_n = len(graphs)
labels = [str(i) for i in range(graphs_n)]
edge_lengths = []
distances = []
for i in range(graphs_n):
   g = graphs[i]
   v1, v2 = g.getRelevantVertices()
   edge = v1 + ":" + v2
   g.drawEdge(v1, v2)
   edge_length = g.getLength(edge)
   edge_lengths.append(edge_length)
   distance = getDistance(v1, v2)
   distances.append(distance)

for i in range(graphs_n):
   mini = edge_lengths[i]
   mini_index = i
   for j in range(i+1, graphs_n):
      if edge_lengths[j] < mini:
         mini = edge_lengths[j]
         mini_index = j
   labels[i], labels[mini_index] = labels[mini_index], labels[i]
   edge_lengths[i], edge_lengths[mini_index] = edge_lengths[mini_index], edge_lengths[i]
   distances[i], distances[mini_index] = distances[mini_index], distances[i]

x = np.arange(len(labels))
width = 0.35  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, edge_lengths, width, label='Edge_Lengths')
rects2 = ax.bar(x + width/2, distances, width, label='3D-Distances')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Amount')
ax.set_title('Graphs')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

def autolabel(rects):
    for rect in rects:
        height = int(rect.get_height())
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)

fig.tight_layout()

plt.show()

