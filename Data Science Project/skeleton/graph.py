import copy
import json
import math
import numpy as np
import os, os.path
import sys
import matplotlib
import matplotlib.pyplot as plt
import sys
import mpl_toolkits.mplot3d as plt3d
from mpl_toolkits.mplot3d import Axes3D

# Static Functions
def getVertexPoints(vertex):
   points = []
   starts = []
   endings = []
   commas = []
   counter = 0
   while counter < len(vertex):
      if vertex[counter] == ',':
         commas.append(counter)
      elif vertex[counter] == '(':
         starts.append(counter)
      elif vertex[counter] == ')':
         endings.append(counter)
      counter += 1
   for i in range(len(starts)):
      x_coord = float(vertex[starts[i]+1:commas[2*i]])
      y_coord = float(vertex[commas[2*i]+2:commas[2*i+1]])
      z_coord = float(vertex[commas[2*i+1]+2:endings[i]])
      points.append((x_coord, y_coord, z_coord))
   return points

def getMiddlePoint(vertex):
   points = getVertexPoints(vertex)
   points_n = len(points)
   x = 0.0
   y = 0.0
   z = 0.0
   for point in points:
      x += point[0]
      y += point[1]
      z += point[2]
   return (x/points_n, y/points_n, z/points_n)

def getEdgeVertices(edge):
   counter = 0
   while counter < len(edge):
      if edge[counter] == ":":
         break
      counter += 1
   return (edge[:counter], edge[counter+1:])

def getEdgePoints(edge_slabs):
   edge_slabs = edge_slabs[1:len(edge_slabs)-1]
   points = []
   starts = []
   endings = []
   commas = []
   counter = 0
   checked = True
   while counter < len(edge_slabs):
      if edge_slabs[counter] == ',':
         if not checked:
            checked = True
         else:
            commas.append(counter)
            if len(commas) % 2 == 0:
               checked = False
      elif edge_slabs[counter] == '(':
         starts.append(counter)
      elif edge_slabs[counter] == ')':
         endings.append(counter)
      counter += 1
   for i in range(len(starts)):
      x_coord = float(edge_slabs[starts[i]+1:commas[2*i]])
      y_coord = float(edge_slabs[commas[2*i]+2:commas[2*i+1]])
      z_coord = float(edge_slabs[commas[2*i+1]+2:endings[i]])
      points.append((x_coord, y_coord, z_coord))
   return points

def getDistance(v1, v2):
   x1, y1, z1 = getMiddlePoint(v1)
   x2, y2, z2 = getMiddlePoint(v2)   
   return math.sqrt(math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2) + math.pow(z2 - z1, 2)* 1.0) 

# Functions to construct the Graph
class Graph:
   def __init__(self, data=""):
      if data == "":
         self.root = ""
         self.vertices = {}
         self.edges = {}
         self.information = {}
         return
      skeleton = json.loads(data)
      self.root = skeleton["root"]
      vertices_skel = skeleton["vertices"]
      self.vertices = {}
      for vertex in vertices_skel:
         self.vertices[vertex["points"]] = (vertex["type"], vertex["lobe"])
      edges_skel = skeleton["edges"]
      self.edges = {}
      for edge in edges_skel:
         slabs = getEdgePoints(edge["points"])
         self.edges[edge["vertex1"] + ":" + edge["vertex2"]] = (edge["length"], slabs)
      self.removeLoops()
      self.removeEndPoints(iterations=1)
      self.information = {}

   def removeLoops(self):
      bfs = self.BFS()
      predecessors = bfs[1]
      edges = self.getEdges()
      for edge in edges:
         v1, v2 = getEdgeVertices(edge)
         if v1 == v2:
            neighbors = self.getNeighbors(v1)
            while v1 in neighbors:
               neighbors.remove(v1)
            checked = False
            for neighbor in neighbors:
               if self.isPredecessor(v1, neighbor, bfs):
                  checked = True
                  break
            if not checked:
               self.vertices[v1] = ("endpoint", self.getLobe(v1))
            slabs = self.getSlabs(edge)
            length = len(slabs)
            if length > 0:
               end = str(slabs[length-1])
               new_edge = v1 + ":" + end
               self.vertices[end] = ("endpoint", self.getLobe(v1))
               self.edges[new_edge] = (self.getLength(edge) - getDistance(v1, end), self.getSlabs(edge)[:length-1])
            del self.edges[edge]

   def removeEndPoints(self, iterations=1, length=math.inf):
      bfs = self.BFS()
      for i in range(iterations):
         if self.getVertices_n() <= 0:
            return
         vertices = self.getVertices()
         endpoints = []
         for vertex in vertices:
            neighbors = self.getNeighbors(vertex)
            while vertex in neighbors:
               neighbors.remove(vertex)
            if len(neighbors) <= 1 and vertex != self.root:
               endpoints.append(vertex)
            else:
               checked = False
               for neighbor in neighbors:
                  if self.isPredecessor(vertex, neighbor, bfs):
                     checked = True
                     break
               if not checked:
                  endpoints.append(vertex)
         change = False
         for endpoint in endpoints:
            edges = self.getEdges()
            for edge in edges:
               v1, v2 = getEdgeVertices(edge)
               if v1 == endpoint or v2 == endpoint:
                  if self.getLength(edge) < length:
                     del self.edges[edge]
                     change = True
         for endpoint in endpoints:
            neighbors = self.getNeighbors(endpoint)
            if len(neighbors) <= 0:
               del self.vertices[endpoint]
         if not change:
            break
      junctions = self.getJunctions()
      for vertex in junctions:
         checked = False
         neighbors = self.getNeighbors(vertex)
         for neighbor in neighbors:
            if self.isPredecessor(vertex, neighbor, bfs):
               checked = True
               break
         if not checked:
            self.vertices[vertex] = ("endpoint", self.getLobe(vertex))

# Simple getter-Functions
   def getVertices(self):
      return list(self.vertices.keys())

   def getVertices_n(self):
      return len(self.getVertices())

   def getJunctions(self):
      junctions = []
      for vertex in self.getVertices():
         if self.getType(vertex) == "junction":
            junctions.append(vertex)
      return junctions
   
   def getJunctions_n(self):
      return len(self.getJunctions())

   def getEndPoints(self):
      endpoints = []
      for vertex in self.getVertices():
         if self.getType(vertex) == "endpoint":
            endpoints.append(vertex)
      return endpoints

   def getEndPoints_n(self):
      return len(self.getEndPoints())

   def getRoot(self):
      return self.root

   def getType(self, vertex):
      return self.vertices[vertex][0]

   def getLobe(self, vertex):
      return self.vertices[vertex][1]

   def getNeighbors(self, vertex):
      vertices = []
      edges = self.getEdges()
      for edge in edges:
         v1, v2 = getEdgeVertices(edge)
         if vertex == v1:
            vertices.append(v2)
         if vertex == v2:
            vertices.append(v1)
      return vertices

   def getSuccessors(self, source, bfs=None):
      successors = []
      if bfs is None:
         bfs = self.BFS()
      vertices = self.getVertices()
      for vertex in vertices:
         if self.isPredecessor(source, vertex, bfs):
            successors.append(vertex)
      return successors

   def getEdges(self):
      return list(self.edges.keys())

   def getEdges_n(self):
      return len(self.getEdges())

   def getSlabs(self, edge):
      edges = self.getEdges()
      if edge not in edges:
         v1, v2 = getEdgeVertices(edge)
         edge = v2 + ":" + v1
      return self.edges[edge][1]

   def getSlabs_n(self, edge):
      edges = self.getEdges()
      if edge not in edges:
         v1, v2 = getEdgeVertices(edge)
         edge = v2 + ":" + v1
      return len(self.getSlabs(edge))

   def getLength(self, edge):
      edges = self.getEdges()
      if edge not in edges:
         v1, v2 = getEdgeVertices(edge)
         edge = v2 + ":" + v1
      return self.edges[edge][0]

   def getMaxLengthEdge(self):
      max_length = 0.0
      max_edge = ""
      edges = self.getEdges()
      for edge in edges:
         length = self.getLength(edge)
         if length > max_length:
            max_length = length
            max_edge = edge
      return max_edge

   def getMaxLength(self):
      return self.getLength(self.getMaxLengthEdge())

   def getSumLength(self):
      sum_length = 0.0
      edges = self.getEdges()
      for edge in edges:
         length = self.getLength(edge)
         sum_length += length
      return sum_length

   def getAvgLength(self):
      sum_length = 0.0
      edges = self.getEdges()
      for edge in edges:
         length = self.getLength(edge)
         sum_length += length
      return (sum_length/len(edges))

   def dijkstra_search(self, source=""):
      edges = self.getEdges()
      if source == "":
         source = self.getRoot()
      distances = {vertex: float('inf') for vertex in self.getVertices()}
      predecessors = {vertex: None for vertex in self.getVertices()}
      distances[source] = 0.0
      vertices = self.getVertices().copy()
      while vertices:
         current_vertex = min(vertices, key=lambda vertex: distances[vertex])
         if distances[current_vertex] == float('inf'):
            break
         neighbors = self.getNeighbors(current_vertex)
         for neighbor in neighbors:
            cost = 0.0
            edge1 = current_vertex + ":" + neighbor
            edge2 = neighbor + ":" + current_vertex
            if edge1 in edges:
               cost = self.getLength(edge1)
            elif edge2 in edges:
               cost = self.getLength(edge2)  
            alternative_route = distances[current_vertex] + cost
            if alternative_route < distances[neighbor]:
               distances[neighbor] = alternative_route
               predecessors[neighbor] = current_vertex
         vertices.remove(current_vertex)
      return (distances, predecessors)

   def getHighestDistanceVertex(self, source=""):
      distances, predecessors = self.dijkstra_search(source)
      max_vertex = None
      max_distance = 0.0
      for vertex in self.getVertices():
         distance = distances[vertex]
         if distance > max_distance:
            max_vertex = vertex
            max_distance = distance
      return max_vertex

   def getHighestDistance(self, source=""):
      distances, predecessors = self.dijkstra_search(source)
      max_vertex = None
      max_distance = 0.0
      for vertex in self.getVertices():
         distance = distances[vertex]
         if distance > max_distance:
            max_vertex = vertex
            max_distance = distance
      return max_distance

   def getAvgDistance(self, source=""):
      distances, predecessors = self.dijkstra_search(source)
      sum_distance = 0.0
      for vertex in self.getVertices():
         sum_distance += distances[vertex]
      return (sum_distance/(self.getVertices_n()-1))

   def getDiameterRadius(self):
      vertices = self.getVertices()
      highest_distances = []
      for vertex in vertices:
         highest_distances.append(self.getMaxDistance(vertex))
      highest_distances = np.asarray(highest_distances)
      return (np.max(highest_distances), np.min(highest_distances))

# Visualizations
   def visualize(self, colored_edge=""):
      edges = self.getEdges()
      if colored_edge != "":
         if colored_edge not in edges:
            v1, v2 = getEdgeVertices(colored_edge)
            colored_edge = v2 + ":" + v1
      vertex_x, vertex_y, vertex_z, slab_x, slab_y, slab_z = [], [], [], [], [], []
      coloredslab_x, coloredslab_y, coloredslab_z = [], [], []
      vertices = self.getVertices()
      for vertex in vertices:
         points = getVertexPoints(vertex)
         for point in points: 
            vertex_x.append(point[0])
            vertex_y.append(point[1])
            vertex_z.append(point[2])
      for edge in edges:
         slabs = self.getSlabs(edge)
         if edge == colored_edge:
            for slab in slabs:
               coloredslab_x.append(slab[0])
               coloredslab_y.append(slab[1])
               coloredslab_z.append(slab[2])
            continue
         for slab in slabs:
            slab_x.append(slab[0])
            slab_y.append(slab[1])
            slab_z.append(slab[2])
      colored_max_x, colored_max_y, colored_max_z = 0.0, 0.0, 0.0
      if len(coloredslab_x) > 0:
         coloredslab_x, coloredslab_y, coloredslab_z = np.asarray(coloredslab_x), np.asarray(coloredslab_y), np.asarray(coloredslab_z)
         colored_max_x, colored_max_y, colored_max_z = np.max(coloredslab_x), np.max(coloredslab_y), np.max(coloredslab_z)
      vertex_x, vertex_y, vertex_z = np.asarray(vertex_x), np.asarray(vertex_y), np.asarray(vertex_z)
      slab_x, slab_y, slab_z = np.asarray(slab_x), np.asarray(slab_y), np.asarray(slab_z)
      max_x = np.max([np.max(vertex_x), np.max(slab_x), colored_max_x])
      max_y = np.max([np.max(vertex_y), np.max(slab_y), colored_max_y])
      max_z = np.max([np.max(vertex_z), np.max(slab_z), colored_max_z])
      max_all = np.max([max_x, max_y, max_z])
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      ax.scatter(vertex_x, vertex_y, vertex_z, s = 3.0, c='blue', marker='s')
      ax.scatter(slab_x, slab_y, slab_z, c='green', s = 1.0, marker='o')
      ax.scatter(coloredslab_x, coloredslab_y, coloredslab_z, c='red', s = 1.0, marker='o')
      ax.set_xlim3d(0, max_all)
      ax.set_ylim3d(0, max_all)
      ax.set_zlim3d(0, max_all)
      ax.set_xlabel('X Label in mm')
      ax.set_ylabel('Y Label in mm')
      ax.set_zlabel('Z Label in mm')
      plt.show()

   def visualizeVertices(self, colored_vertices=[]):
      edges = self.getEdges()
      vertex_x, vertex_y, vertex_z, slab_x, slab_y, slab_z = [], [], [], [], [], []
      coloredvertex_x, coloredvertex_y, coloredvertex_z = [], [], []
      vertices = self.getVertices()
      for vertex in vertices:
         points = getVertexPoints(vertex)
         if vertex in colored_vertices:
            for point in points: 
               coloredvertex_x.append(point[0])
               coloredvertex_y.append(point[1])
               coloredvertex_z.append(point[2])
         else:
            for point in points: 
               vertex_x.append(point[0])
               vertex_y.append(point[1])
               vertex_z.append(point[2])
      for edge in edges:
         slabs = self.getSlabs(edge)
         for slab in slabs:
            slab_x.append(slab[0])
            slab_y.append(slab[1])
            slab_z.append(slab[2])
      colored_max_x, colored_max_y, colored_max_z = 0.0, 0.0, 0.0
      if len(coloredvertex_x) > 0:
         coloredvertex_x, coloredvertex_y, coloredvertex_z = np.asarray(coloredvertex_x), np.asarray(coloredvertex_y), np.asarray(coloredvertex_z)
         colored_max_x, colored_max_y, colored_max_z = np.max(coloredvertex_x), np.max(coloredvertex_y), np.max(coloredvertex_z)
      vertex_x, vertex_y, vertex_z = np.asarray(vertex_x), np.asarray(vertex_y), np.asarray(vertex_z)
      slab_x, slab_y, slab_z = np.asarray(slab_x), np.asarray(slab_y), np.asarray(slab_z)
      max_x = np.max([np.max(vertex_x), np.max(slab_x), colored_max_x])
      max_y = np.max([np.max(vertex_y), np.max(slab_y), colored_max_y])
      max_z = np.max([np.max(vertex_z), np.max(slab_z), colored_max_z])
      max_all = np.max([max_x, max_y, max_z])
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      ax.scatter(vertex_x, vertex_y, vertex_z, s = 2.0, c='blue', marker='s')
      ax.scatter(slab_x, slab_y, slab_z, c='green', s = 1.0, marker='o')
      ax.scatter(coloredvertex_x, coloredvertex_y, coloredvertex_z, c='red', s = 6.0, marker='s')
      ax.set_xlim3d(0, max_all)
      ax.set_ylim3d(0, max_all)
      ax.set_zlim3d(0, max_all)
      ax.set_xlabel('X Label in mm')
      ax.set_ylabel('Y Label in mm')
      ax.set_zlabel('Z Label in mm')
      plt.show()
   
   def drawLines(self, colored_edge="", color_lobes=True):
      edges = self.getEdges()
      if colored_edge != "":
         if colored_edge not in edges:
            v1, v2 = getEdgeVertices(colored_edge)
            colored_edge = v2 + ":" + v1
      vertex_x, vertex_y, vertex_z = [], [], []
      vertices = self.getVertices()
      for vertex in vertices:
         points = getVertexPoints(vertex)
         for point in points: 
            vertex_x.append(point[0])
            vertex_y.append(point[1])
            vertex_z.append(point[2])
      vertex_x, vertex_y, vertex_z = np.asarray(vertex_x), np.asarray(vertex_y), np.asarray(vertex_z)
      max_x, max_y, max_z = np.max(vertex_x), np.max(vertex_x), np.max(vertex_x)
      max_all = np.max([max_x, max_y, max_z])
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      ax.set_xlim3d(0, max_all)
      ax.set_ylim3d(0, max_all)
      ax.set_zlim3d(0, max_all)
      ax.set_xlabel('X Label in mm')
      ax.set_ylabel('Y Label in mm')
      ax.set_zlabel('Z Label in mm')
      for edge in edges:
         v1, v2 = getEdgeVertices(edge)
         p1, p2 = getMiddlePoint(v1), getMiddlePoint(v2)
         if edge == colored_edge:
            xs = p1[0], p2[0] 
            ys = p1[1], p2[1]
            zs = p1[2], p2[2]
            line = plt3d.art3d.Line3D(xs, ys, zs, c = "red", lw=3)
            ax.add_line(line)
            continue
         if color_lobes:
            colors = {"none":"black", "LeftUpperLobe":"green", "LeftLowerLobe":"orange", "RightUpperLobe":"blue", "RightLowerLobe":"magenta", "RightMiddleLobe":"brown"}  
            t1, t2 = self.getLobe(v1), self.getLobe(v2)
            pm = (p1[0]+p2[0])/2, (p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 
            xs = p1[0], pm[0] 
            ys = p1[1], pm[1]
            zs = p1[2], pm[2]
            line = plt3d.art3d.Line3D(xs, ys, zs, c = colors[t1])
            ax.add_line(line)
            xs = pm[0], p2[0] 
            ys = pm[1], p2[1]
            zs = pm[2], p2[2]
            line = plt3d.art3d.Line3D(xs, ys, zs, c = colors[t2])
            ax.add_line(line)
         else:
            xs = p1[0], p2[0] 
            ys = p1[1], p2[1]
            zs = p1[2], p2[2]
            line = plt3d.art3d.Line3D(xs, ys, zs)
            ax.add_line(line)
      plt.show()

   def drawLinesVertices(self, colored_vertices=[], color_lobes=True):
      vertex_x, vertex_y, vertex_z = [], [], []
      vertices = self.getVertices()
      edges = self.getEdges()
      for vertex in vertices:
         points = getVertexPoints(vertex)
         for point in points: 
            vertex_x.append(point[0])
            vertex_y.append(point[1])
            vertex_z.append(point[2])
      vertex_x, vertex_y, vertex_z = np.asarray(vertex_x), np.asarray(vertex_y), np.asarray(vertex_z)
      max_x, max_y, max_z = np.max(vertex_x), np.max(vertex_y), np.max(vertex_z)
      max_all = np.max([max_x, max_y, max_z])
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      ax.set_xlim3d(0, max_all)
      ax.set_ylim3d(0, max_all)
      ax.set_zlim3d(0, max_all)
      ax.set_xlabel('X Label in mm')
      ax.set_ylabel('Y Label in mm')
      ax.set_zlabel('Z Label in mm')
      for edge in edges:
         v1, v2 = getEdgeVertices(edge)
         p1, p2 = getMiddlePoint(v1), getMiddlePoint(v2)
         if color_lobes:
            colors = {"none":"black", "LeftUpperLobe":"green", "LeftLowerLobe":"orange", "RightUpperLobe":"blue", "RightLowerLobe":"magenta", "RightMiddleLobe":"brown"}  
            t1, t2 = self.getLobe(v1), self.getLobe(v2)
            pm = (p1[0]+p2[0])/2, (p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 
            xs = p1[0], pm[0] 
            ys = p1[1], pm[1]
            zs = p1[2], pm[2]
            line = plt3d.art3d.Line3D(xs, ys, zs, c = colors[t1])
            ax.add_line(line)
            xs = pm[0], p2[0] 
            ys = pm[1], p2[1]
            zs = pm[2], p2[2]
            line = plt3d.art3d.Line3D(xs, ys, zs, c = colors[t2])
            ax.add_line(line)
         else:
            xs = p1[0], p2[0] 
            ys = p1[1], p2[1]
            zs = p1[2], p2[2]
            line = plt3d.art3d.Line3D(xs, ys, zs)
            ax.add_line(line)
      for colored_vertex in colored_vertices:
         colored_vertex = getVertexPoints(colored_vertex)[0]
         vertex_x, vertex_y, vertex_z = np.array([colored_vertex[0]]), np.array([colored_vertex[1]]), np.array([colored_vertex[2]])
         ax.scatter(vertex_x, vertex_y, vertex_z, s = 6, c='red', marker='s')
      plt.show()

   def visualizeLobes(self):
      none_x, none_y, none_z, ll_x, ll_y, ll_z, lu_x, lu_y, lu_z = [], [], [], [], [], [], [], [], []
      rl_x, rl_y, rl_z, rm_x, rm_y, rm_z, ru_x, ru_y, ru_z = [], [], [], [], [], [], [], [], []
      vertices = self.getVertices()
      for vertex in vertices:
         points = getVertexPoints(vertex)
         if self.getLobe(vertex) == "LeftLowerLobe":
            for point in points:
               ll_x.append(point[0])
               ll_y.append(point[1])
               ll_z.append(point[2])
         elif self.getLobe(vertex) == "LeftUpperLobe":
            for point in points:
               lu_x.append(point[0])
               lu_y.append(point[1])
               lu_z.append(point[2])
         elif self.getLobe(vertex) == "RightLowerLobe":
            for point in points:
               rl_x.append(point[0])
               rl_y.append(point[1])
               rl_z.append(point[2])
         elif self.getLobe(vertex) == "RightMiddleLobe":
            for point in points:
               rm_x.append(point[0])
               rm_y.append(point[1])
               rm_z.append(point[2])
         elif self.getLobe(vertex) == "RightUpperLobe":
            for point in points:
               ru_x.append(point[0])
               ru_y.append(point[1])
               ru_z.append(point[2])
         else:
            for point in points:
               none_x.append(point[0])
               none_y.append(point[1])
               none_z.append(point[2])
      none_x, none_y, none_z = np.asarray(none_x), np.asarray(none_y), np.asarray(none_z) 
      ll_x, ll_y, ll_z = np.asarray(ll_x), np.asarray(ll_y), np.asarray(ll_z)
      lu_x, lu_y, lu_z = np.asarray(lu_x), np.asarray(lu_y), np.asarray(lu_z)
      rl_x, rl_y, rl_z = np.asarray(rl_x), np.asarray(rl_y), np.asarray(rl_z)
      rm_x, rm_y, rm_z = np.asarray(rm_x), np.asarray(rm_y), np.asarray(rm_z)
      ru_x, ru_y, ru_z = np.asarray(ru_x), np.asarray(ru_y), np.asarray(ru_z)
      none_max_x, none_max_y, none_max_z, ll_max_x, ll_max_y, ll_max_z= 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
      lu_max_x, lu_max_y, lu_max_z, rl_max_x, rl_max_y, rl_max_z = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
      rm_max_x, rm_max_y, rm_max_z, ru_max_x, ru_max_y, ru_max_z = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
      if len(none_x) > 0:
         none_max_x, none_max_y, none_max_z = np.max(none_x), np.max(none_y), np.max(none_z)
      if len(ll_x) > 0:
         ll_max_x, ll_max_y, ll_max_z = np.max(ll_x), np.max(ll_y), np.max(ll_z)
      if len(lu_x) > 0:
         lu_max_x, lu_max_y, lu_max_z = np.max(lu_x), np.max(lu_y), np.max(lu_z) 
      if len(rl_x) > 0:
         rl_max_x, rl_max_y, rl_max_z = np.max(rl_x), np.max(rl_y), np.max(rl_z) 
      if len(rm_x) > 0:
         rm_max_x, rm_max_y, rm_max_z = np.max(rm_x), np.max(rm_y), np.max(rm_z) 
      if len(ru_x) > 0:
         ru_max_x, ru_max_y, ru_max_z = np.max(ru_x), np.max(ru_y), np.max(ru_z)   
      max_x = np.max([none_max_x, ll_max_x, lu_max_x, rl_max_x, rm_max_x, ru_max_x])
      max_y = np.max([none_max_y, ll_max_y, lu_max_y, rl_max_y, rm_max_y, ru_max_y])
      max_z = np.max([none_max_z, ll_max_z, lu_max_z, rl_max_z, rm_max_z, ru_max_z])
      max_all = np.max([max_x, max_y, max_z])
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      ax.scatter(none_x, none_y, none_z, s = 8, c='black', marker='s')
      ax.scatter(ll_x, ll_y, ll_z, s = 8, c='orange', marker='s')
      ax.scatter(lu_x, lu_y, lu_z, s = 8, c='green', marker='s')
      ax.scatter(rl_x, rl_y, rl_z, s = 8, c='magenta', marker='s')
      ax.scatter(rm_x, rm_y, rm_z, s = 8, c='brown', marker='s')
      ax.scatter(ru_x, ru_y, ru_z, s = 8, c='blue', marker='s')
      ax.set_xlim3d(0, max_all)
      ax.set_ylim3d(0, max_all)
      ax.set_zlim3d(0, max_all)
      ax.set_xlabel('X Label in mm')
      ax.set_ylabel('Y Label in mm')
      ax.set_zlabel('Z Label in mm')
      edges = self.getEdges()
      for edge in edges:
         pair = getEdgeVertices(edge)
         v1, v2 = getMiddlePoint(pair[0]), getMiddlePoint(pair[1])
         xs = v1[0], v2[0] 
         ys = v1[1], v2[1]
         zs = v1[2], v2[2]
         line = plt3d.art3d.Line3D(xs, ys, zs, c='grey', lw=0.5)
         ax.add_line(line)
      plt.show()

# Functions to construct the graph of a single lobe
   def BFS(self, source=""): 
     distances = {vertex: -1 for vertex in self.getVertices()}
     predecessors = {vertex: None for vertex in self.getVertices()}
     queue = [] 
     if source == "":
         source = self.getRoot()
     queue.append(source)
     distances[source] = 0
     while queue: 
         vertex = queue.pop(0) 
         neighbors = self.getNeighbors(vertex)
         for neighbor in neighbors: 
                if distances[neighbor] == -1: 
                    queue.append(neighbor)
                    distances[neighbor] = distances[vertex] + 1
                    predecessors[neighbor] = vertex
     return (distances, predecessors)

   def isPredecessor(self, v1, v2, bfs=None):
     if bfs is None:
        bfs = self.BFS()
     predecessors = bfs[1]
     predecessor = v2
     while predecessor is not None:
        if predecessor == v1:
           return True
        predecessor = predecessors[predecessor]
     return False

   def allPredecessorsOfList(self, vertices, bfs, allowed_failures=0):
      all_predecessors = []
      junctions = self.getJunctions()
      for junction in junctions:
         fail_counter = 0
         for vertex in vertices:
            if not self.isPredecessor(junction, vertex, bfs):
               fail_counter += 1
         if fail_counter <= allowed_failures:
            all_predecessors.append(junction)
      return all_predecessors
 
   def lowestPredecessorOfList(self, vertices, allowed_failures=0):
      bfs = self.BFS()
      all_predecessors = self.allPredecessorsOfList(vertices, bfs, allowed_failures)
      if len(all_predecessors) == 0:
         return None
      distances = bfs[0]
      lowest = distances[all_predecessors[0]]
      lowest_vertex = all_predecessors[0]
      for i in range(1, len(all_predecessors)):
         vertex = all_predecessors[i]
         distance = distances[vertex]
         if distance > lowest:
            lowest = distance
            lowest_vertex = vertex
      return lowest_vertex

   def getLobeStart(self, lobe):
      bfs = self.BFS()
      vertices = self.getVertices()
      lobes = ["LeftUpperLobe", "LeftLowerLobe", "RightUpperLobe", "RightMiddleLobe", "RightLowerLobe"]
      all_lobes = [vertex for vertex in vertices if self.getLobe(vertex) in lobes]
      v_all = self.lowestPredecessorOfList(all_lobes, 0)
      if lobe == "LeftUpperLobe" or lobe == "LeftLowerLobe":
         left_lobes = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[:2]]
         v_left = self.lowestPredecessorOfList(left_lobes, 0)
         if lobe == "LeftUpperLobe":
            left_upper = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[0]]
            v_left_upper = self.lowestPredecessorOfList(left_upper, 0)
            counter = 1
            while v_left_upper == v_left:
               v_left_upper = self.lowestPredecessorOfList(left_upper, counter)
               counter += 1
            return v_left_upper
         if lobe == "LeftLowerLobe":
            left_lower = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[1]]
            v_left_lower = self.lowestPredecessorOfList(left_lower, 0)
            counter = 1
            while v_left_lower == v_left:
               v_left_lower = self.lowestPredecessorOfList(left_lower, counter)
               counter += 1
            return v_left_lower
      if lobe == "RightUpperLobe" or lobe == "RightMiddleLobe" or lobe == "RightLowerLobe":
         right_lobes = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[2:]]
         v_right = self.lowestPredecessorOfList(right_lobes, 0)
         if lobe == "RightUpperLobe":
            right_upper = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[2]]
            v_right_upper = self.lowestPredecessorOfList(right_upper, 0)
            counter = 1
            while v_right_upper == v_right:
               v_right_upper = self.lowestPredecessorOfList(right_upper, counter)
               counter += 1
            return v_right_upper
         if lobe == "RightMiddleLobe":
            right_middle = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[3]]
            v_right_middle = self.lowestPredecessorOfList(right_middle, 0)
            counter = 1
            while v_right_middle == v_right:
               v_right_middle = self.lowestPredecessorOfList(right_middle, counter)
               counter += 1
            return v_right_middle
         if lobe == "RightLowerLobe":
            right_lower = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[4]]
            v_right_lower = self.lowestPredecessorOfList(right_lower, 0)
            counter = 1
            while v_right_lower == v_right:
               v_right_lower = self.lowestPredecessorOfList(right_lower, counter)
               counter += 1
            return v_right_lower

   def buildLobe(self, lobe):
      g = Graph("")
      start = self.getLobeStart(lobe)
      g.root = start
      bfs = self.BFS()
      predecessors = bfs[1]
      vertices = self.getVertices()
      for vertex in vertices:
         if self.isPredecessor(start, vertex, bfs):
            g.vertices[vertex] = self.vertices[vertex]
      new_vertices = g.getVertices()
      edges = self.getEdges()
      for edge in edges:
         v1, v2 = getEdgeVertices(edge)
         if v1 in new_vertices and v2 in new_vertices:
            g.edges[edge] = self.edges[edge]
      return g

# Remaining Functions
   def compareLengths(self):
      edges = self.getEdges()
      lengths = []
      distances = []
      for edge in edges:
         v1, v2 = getEdgeVertices(edge)
         length = self.getLength(edge)
         distance = getDistance(v1, v2)
         if length < distance:
            lengths.append(length)
            distances.append(distance)
      if len(lengths) == 0:
         return
      labels = [str(i) for i in range(len(lengths))]
      x = np.arange(len(labels))
      width = 0.35  # the width of the bars
      fig, ax = plt.subplots()
      rects1 = ax.bar(x - width/2, lengths, width, label='Edge_Lengths')
      rects2 = ax.bar(x + width/2, distances, width, label='3D-Distances')
      ax.set_ylabel('Amount')
      ax.set_title('Edges')
      ax.set_xticks(x)
      ax.set_xticklabels(labels)
      ax.legend()

      def autolabel(rects):
         for rect in rects:
            height = int(rect.get_height())
            ax.annotate('{}'.format(height),
               xy=(rect.get_x() + rect.get_width() / 2, height),
               xytext=(0, 3),  # 3 points vertical offset
               textcoords="offset points",
               ha='center', va='bottom')
      autolabel(rects1)
      autolabel(rects2)
      fig.tight_layout()
      plt.show()

   def calculateLength(self, v1, v2, points_string):
      length = 0.0
      start = v1
      points_string = getEdgePoints(points_string)
      for slab in points_string:
         length += getDistance(start, str(slab))
         start = str(slab)
      length += getDistance(start, v2)
      return length 

   def getRelevantVertices(self):
      vertices = self.getVertices()
      lobes = ["LeftUpperLobe", "LeftLowerLobe", "RightUpperLobe", "RightMiddleLobe", "RightLowerLobe"]
      all_lobes = [vertex for vertex in vertices if self.getLobe(vertex) in lobes]
      v_all = self.lowestPredecessorOfList(all_lobes, 0)
      left_lobes = [vertex for vertex in vertices if self.getLobe(vertex) in lobes[:2]]
      v_left = self.lowestPredecessorOfList(left_lobes, 0)
      return (v_all, v_left)

   def drawEdge(self, upper, lower):
      bfs = self.BFS()
      distances, predecessors = bfs
      if distances[lower] < distances[upper]:
         lower, upper = upper, lower
      old_predecessor = lower
      predecessor = predecessors[lower]
      edges = self.getEdges()
      new_edge = upper + ":" + lower
      new_length = 0.0
      new_slabs = []
      while old_predecessor != upper:
         edge = predecessor + ":" + old_predecessor
         if edge not in edges:
            edge = old_predecessor + ":" + predecessor
         new_length += self.getLength(edge)
         new_slabs += self.getSlabs(edge)
         if predecessor != upper:
            new_slabs.append(getMiddlePoint(predecessor))
         tmp = predecessor
         old_predecessor = predecessor
         predecessor = predecessors[tmp]
         del self.edges[edge]
      self.edges[new_edge] = (new_length, new_slabs)

if __name__== "__main__": 

   path = "patients/good/"
   patient = sys.argv[1]
   f = open(path + patient + ".json", "r")
   data = f.read()
   f.close()
   g = Graph(data)
   g.visualize()
   g.drawLines()
   g.visualizeLobes()
   
