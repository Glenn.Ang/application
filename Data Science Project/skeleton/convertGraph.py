import json
import math
import numpy as np
import os, os.path
import pydicom
import shutil
from shutil import copy
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# enter the location of the patient-directory that contains the folders of the 5 different lobes and the .json-file
path = "patients/"

class Graph:
   def __init__(self, data, patient):
      self.root = skeleton["root"]
      vertices_skel = skeleton["vertices"]
      self.vertices = {}
      for vertex in vertices_skel:
         self.vertices[vertex["points"]] = (vertex["type"], vertex["lobe"])
      edges_skel = skeleton["edges"]
      self.edges = {}
      for edge in edges_skel:
         self.edges[edge["vertex1"] + ":" + edge["vertex2"]] = (edge["length"], edge["points"])
      lobes = ["LeftLowerLobe", "LeftUpperLobe", "RightLowerLobe", "RightMiddleLobe", "RightUpperLobe"]
      self.vertice_lobes = {}
      vertices = self.getVertices()
      for vertex in vertices:
         vertex_lobes = [0, 0, 0, 0, 0] 
         point = getVertexPoints(vertex)[0]
         x = int(point[1])
         y = int(point[0])
         z = int(point[2]+1)
         for i in range(5):
            lobe = lobes[i]
            file = path + lobe + "/IMG" + str(z)
            ds = pydicom.dcmread(file)
            pix = ds.pixel_array
            for k in range(7):
               if x-k >= 0:
                  if pix[x-k][y] != -10000:
                     vertex_lobes[i] += 1
                     if y-k >= 0:
                        if pix[x-k][y-k] != -10000:
                           vertex_lobes[i] += 1
                     if y+k <= 511:
                        if pix[x-k][y+k] != -10000:
                           vertex_lobes[i] += 1
               if x+k <= 511:
                  if pix[x+k][y] != -10000:
                     vertex_lobes[i] += 1
                     if y-k >= 0:
                        if pix[x+k][y-k] != -10000:
                           vertex_lobes[i] += 1
                     if y+k <= 511:
                        if pix[x+k][y+k] != -10000:
                           vertex_lobes[i] += 1
               if y-k >= 0:
                  if pix[x][y-k] != -10000:
                     vertex_lobes[i] += 1
                     if x-k >= 0:
                        if pix[x-k][y-k] != -10000:
                           vertex_lobes[i] += 1
                     if x+k <= 511:
                        if pix[x+k][y-k] != -10000:
                           vertex_lobes[i] += 1
               if y+k <= 511:
                  if pix[x][y+k] != -10000:
                     vertex_lobes[i] += 1
                     if x-k >= 0:
                        if pix[x-k][y+k] != -10000:
                           vertex_lobes[i] += 1
                     if x+k <= 511:
                        if pix[x+k][y+k] != -10000:
                           vertex_lobes[i] += 1
         loc = -1
         maxi = 0
         for i in range(5):
            if vertex_lobes[i] > maxi:
               loc = i
               maxi = vertex_lobes[i]
         if loc == -1:
            self.vertice_lobes[vertex] = "none"
         else:
            self.vertice_lobes[vertex] = lobes[loc]
      
   def updateLobes(self, skeleton):
      vertices = list(self.getVertices())
      for i in range(len(vertices)):
         skeleton["vertices"][i]["lobe"] = self.vertice_lobes[skeleton["vertices"][i]["points"]]
      return skeleton

   def getVertices(self):
      return self.vertices.keys()

   def getEdges(self):
      return self.edges.keys()

   def getEdgeSlabs(self, edge):
      return getEdgePoints(self.edges[edge][1])
   
   def getMaxima(self):
      maximum_y = 0.0
      maximum_z = 0.0
      vertices = self.getVertices()
      edges = self.getEdges()
      for vertex in vertices:
         points = getVertexPoints(vertex)
         for point in points:
            if point[1] > maximum_y:
               maximum_y = point[1]
            if point[2] > maximum_z:
               maximum_z = point[2]
      for edge in edges:
         points = self.getEdgeSlabs(edge)
         for point in points:
            if point[1] > maximum_y:
               maximum_y = point[1]
            if point[2] > maximum_z:
               maximum_z = point[2]
      return (maximum_y, maximum_z)

   def changeVertex(self, vertex, maximum_y, maximum_z):
      vertex_string = ''
      points = getVertexPoints(vertex)
      for point in points:
         vertex_string += '(' + str(point[0]) + ', ' + str(511*0.694 - point[1]) + ', ' + str(maximum_z - point[2]) + ') '
      return vertex_string

   def initPoints(self, skeleton):
      root_point = getVertexPoints(skeleton["root"])[0]
      self.root = '(' + str(root_point[0]*0.694) + ', ' + str(root_point[1]*0.694) + ', ' + str(root_point[2]*0.5) + ') '
      vertices_skel = skeleton["vertices"]
      self.vertices = {}   
      for vertex in vertices_skel:
         vertex_string = ''
         points = getVertexPoints(vertex["points"])
         for point in points:
            vertex_string += '(' + str(point[0]*0.694) + ', ' + str(point[1]*0.694) + ', ' + str(point[2]*0.5) + ') '
         self.vertices[vertex_string] = vertex["type"]
      edges_skel = skeleton["edges"]
      self.edges = {}
      for edge in edges_skel:
         edge_string = ''
         points = getVertexPoints(edge["vertex1"])
         for point in points:
            edge_string += '(' + str(point[0]*0.694) + ', ' + str(point[1]*0.694) + ', ' + str(point[2]*0.5) + ') '
         edge_string += ":"
         points = getVertexPoints(edge["vertex2"])
         for point in points:
            edge_string += '(' + str(point[0]*0.694) + ', ' + str(point[1]*0.694) + ', ' + str(point[2]*0.5) + ') '
         points = getEdgePoints(edge["points"])
         counter = 0
         edge_points = '['
         for point in points:
            if counter > 0:
               edge_points += ", "
            edge_points += '(' + str(point[0]*0.694) + ', ' + str(point[1]*0.694) + ', ' + str(point[2]*0.5) + ')'
            counter = 1
         if edge_string in self.edges.keys():
            counter = 0
            edge_string += str(counter)
            while True:
               edge_string = edge_string[:-1]
               counter += 1
               edge_string += str(counter)
               if edge_string not in self.edges.keys():
                  break
         self.edges[edge_string] = (edge["length"], edge_points + ']')

   def updatePoints(self, skeleton):
      maximum_y, maximum_z = self.getMaxima()
      root_point = getVertexPoints(self.root)[0]
      skeleton["root"] = '(' + str(root_point[0]) + ', ' + str(511*0.694 - root_point[1]) + ', ' + str(maximum_z - root_point[2]) + ') ' 
      vertices_string = ''
      vertices = list(self.getVertices())
      for i in range(len(vertices)):
         skeleton["vertices"][i]["points"] = self.changeVertex(vertices[i], maximum_y, maximum_z)
      edges = list(self.getEdges())
      for i in range(len(edges)):
         vertex1, vertex2 = getEdgeVertices(edges[i])
         skeleton["edges"][i]["vertex1"] = self.changeVertex(vertex1, maximum_y, maximum_z)
         skeleton["edges"][i]["vertex2"] = self.changeVertex(vertex2, maximum_y, maximum_z)
         points = getEdgePoints(self.edges[edges[i]][1])
         points_string = '['
         counter = 0
         for point in points:
            if counter > 0:
               points_string += ', '
            points_string += '(' + str(point[0]) + ', ' + str(511*0.694 - point[1]) + ', ' + str(maximum_z - point[2]) + ')'
            counter += 1
         points_string += ']'
         skeleton["edges"][i]["points"] = points_string
         skeleton["edges"][i]["length"] = self.calculateLength(skeleton["edges"][i]["vertex1"], skeleton["edges"][i]["vertex2"], points_string)
      return skeleton

   
   # berechnet die Länge einer Kante durch Durchlaufen aller dazwischenliegenden Voxel
   def calculateLength(self, v1, v2, points_string):
      length = 0.0
      points_string = getEdgePoints(points_string)
      if len(points_string) == 0:
         return getDistance(v1, v2)
      if getDistance(v1, str(points_string[0])) > getDistance(v1, str(points_string[len(points_string)-1])):
         v1, v2 = v2, v1
      start = v1
      for slab in points_string:
         length += getDistance(start, str(slab))
         start = str(slab)
      length += getDistance(start, v2)
      return length   

def getVertexPoints(vertex):
   points = []
   starts = []
   endings = []
   commas = []
   counter = 0
   while counter < len(vertex):
      if vertex[counter] == ',':
         commas.append(counter)
      elif vertex[counter] == '(':
         starts.append(counter)
      elif vertex[counter] == ')':
         endings.append(counter)
      counter += 1
   for i in range(len(starts)):
      x_coord = float(vertex[starts[i]+1:commas[2*i]])
      y_coord = float(vertex[commas[2*i]+2:commas[2*i+1]])
      z_coord = float(vertex[commas[2*i+1]+2:endings[i]])
      points.append((x_coord, y_coord, z_coord))
   return points

def getMiddlePoint(vertex):
   points = getVertexPoints(vertex)
   points_n = len(points)
   x = 0.0
   y = 0.0
   z = 0.0
   for point in points:
      x += point[0]
      y += point[1]
      z += point[2]
   return (x/points_n, y/points_n, z/points_n)

def getEdgeVertices(edge):
   counter = 0
   while counter < len(edge):
      if edge[counter] == ":":
         break
      counter += 1
   return (edge[:counter], edge[counter+1:-1])

def getEdgePoints(edge):
   edge = edge[1:len(edge)-1]
   points = []
   starts = []
   endings = []
   commas = []
   counter = 0
   checked = True
   while counter < len(edge):
      if edge[counter] == ',':
         if not checked:
            checked = True
         else:
            commas.append(counter)
            if len(commas) % 2 == 0:
               checked = False
      elif edge[counter] == '(':
         starts.append(counter)
      elif edge[counter] == ')':
         endings.append(counter)
      counter += 1
   for i in range(len(starts)):
      x_coord = float(edge[starts[i]+1:commas[2*i]])
      y_coord = float(edge[commas[2*i]+2:commas[2*i+1]])
      z_coord = float(edge[commas[2*i+1]+2:endings[i]])
      points.append((x_coord, y_coord, z_coord))
   return points

# berechnet 3D-Entfernung zwischen 2 Knoten
def getDistance(v1, v2):
   x1, y1, z1 = getMiddlePoint(v1)
   x2, y2, z2 = getMiddlePoint(v2)   
   return math.sqrt(math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2) + math.pow(z2 - z1, 2)* 1.0) 

patient = sys.argv[1]
path += patient + "/"
f = open(path + patient + ".json", "r")
data = f.read()
f.close()

f = open(path + patient + ".json", "w+")
skeleton = json.loads(data)
g = Graph(skeleton, patient)
skeleton = g.updateLobes(skeleton)
g.initPoints(skeleton)
skeleton = g.updatePoints(skeleton)
json.dump(skeleton, f)
f.close()

