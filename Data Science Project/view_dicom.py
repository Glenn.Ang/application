#!/usr/bin/env python3
import argparse
import pydicom
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Show dicom images")
parser.add_argument('--filename', required=True, type=str, help="/path/to/dicom")
parser.add_argument('--save', action="store_true", help="save the generated image instead of showing it")
args = parser.parse_args()

filename = args.filename
ds = pydicom.dcmread(filename)

plt.imshow(ds.pixel_array, cmap=plt.cm.bone)

if args.save == True:
    plt.savefig('./fig.png')
else:
    plt.show()
