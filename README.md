# Application

The folder 'Data Science Project' contains the implementation for the module called 'KSWS', in which we were supposed to analyze and visualize the airway structures of different patients. All of these programs were completely written by myself (I removed everything else).

The folder 'Seminar Reports' contains the 2 texts for the module called 'Seminar' (both of them also had to be presented in front of the class at the end of the semester). Sadly, they are only available in German.

The folder 'Exam Results' shows the actual points scored in the 3 modules 'Modeling and Simulation', 'Operating Systems and Distributed Systems' (both fourth semester) and 'Databases 1' (fifth semester). These were actually the only three exams in which this information was made public. The student ID is supposed to show that the number '214204858' really belongs to me.

